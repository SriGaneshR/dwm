/* Applied Patches: */
// colorbar, coolautostart, adjacenttags, movestack, selfrestart, attachbottom

/* Adjacent Tags */
unsigned int nexttag() {
    unsigned int seltag = selmon->tagset[selmon->seltags];
    return seltag == (1 << (LENGTH(tags) - 1)) ? 1 : seltag << 1;
}

unsigned int prevtag() {
    unsigned int seltag = selmon->tagset[selmon->seltags];
    return seltag == 1 ? (1 << (LENGTH(tags) - 1)) : seltag >> 1;
}

unsigned int nextnonemptytag() {
    unsigned int seltag = selmon->tagset[selmon->seltags];
    unsigned int usedtags = 0;
    Client *c = selmon->clients;

    if (!c)
        return seltag;

    do {
        usedtags |= c->tags;
        c = c->next;
    } while (c);

    do {
        seltag = seltag == (1 << (LENGTH(tags) - 1)) ? 1 : seltag << 1;
    } while (!(seltag & usedtags));

    return seltag;
}

unsigned int prevnonemptytag() {
    unsigned int seltag = selmon->tagset[selmon->seltags];
    unsigned int usedtags = 0;
    Client *c = selmon->clients;

    if (!c)
        return seltag;

    do {
        usedtags |= c->tags;
        c = c->next;
    } while (c);

    do {
        seltag = seltag == 1 ? (1 << (LENGTH(tags) - 1)) : seltag >> 1;
    } while (!(seltag & usedtags));

    return seltag;
}

void viewnext(const Arg *arg) {
	view(&(const Arg){.ui = nexttag()});
}

void viewprev(const Arg *arg) {
	view(&(const Arg){.ui = prevtag()});
}

void viewnextnonempty(const Arg *arg) {
	view(&(const Arg){.ui = nextnonemptytag()});
}

void viewprevnonempty(const Arg *arg) {
	view(&(const Arg){.ui = prevnonemptytag()});
}

void tagtonext(const Arg *arg) {
	unsigned int tmp;

	if (selmon->sel == NULL)
		return;

	tmp = nexttag();
	tag(&(const Arg){.ui = tmp });
	view(&(const Arg){.ui = tmp });
}

void tagtoprev(const Arg *arg) {
	unsigned int tmp;

	if (selmon->sel == NULL)
		return;

	tmp = prevtag();
	tag(&(const Arg){.ui = tmp });
	view(&(const Arg){.ui = tmp });
}

void tagtonextnonempty(const Arg *arg) {
	unsigned int tmp;

	if (selmon->sel == NULL)
		return;

	tmp = nextnonemptytag();
	tag(&(const Arg){.ui = tmp });
	view(&(const Arg){.ui = tmp });
}

void tagtoprevnonempty(const Arg *arg) {
	unsigned int tmp;

	if (selmon->sel == NULL)
		return;

	tmp = prevnonemptytag();
	tag(&(const Arg){.ui = tmp });
	view(&(const Arg){.ui = tmp });
}

void movetonext(const Arg *arg) {
	unsigned int tmp;

	if (selmon->sel == NULL)
		return;

	tmp = nexttag();
	tag(&(const Arg){.ui = tmp });
}

void movetoprev(const Arg *arg) {
	unsigned int tmp;

	if (selmon->sel == NULL)
		return;

	tmp = prevtag();
	tag(&(const Arg){.ui = tmp });
}

void movetonextnonempty(const Arg *arg) {
	unsigned int tmp;

	if (selmon->sel == NULL)
		return;

	tmp = nextnonemptytag();
	tag(&(const Arg){.ui = tmp });
}

void movetoprevnonempty(const Arg *arg) {
	unsigned int tmp;

	if (selmon->sel == NULL)
		return;

	tmp = prevnonemptytag();
	tag(&(const Arg){.ui = tmp });
}


/* Move Stack */
void movestack(const Arg *arg) {
	Client *c = NULL, *p = NULL, *pc = NULL, *i;

	if(arg->i > 0) {
		/* find the client after selmon->sel */
		for(c = selmon->sel->next; c && (!ISVISIBLE(c) || c->isfloating); c = c->next);
		if(!c)
			for(c = selmon->clients; c && (!ISVISIBLE(c) || c->isfloating); c = c->next);

	}
	else {
		/* find the client before selmon->sel */
		for(i = selmon->clients; i != selmon->sel; i = i->next)
			if(ISVISIBLE(i) && !i->isfloating)
				c = i;
		if(!c)
			for(; i; i = i->next)
				if(ISVISIBLE(i) && !i->isfloating)
					c = i;
	}
	/* find the client before selmon->sel and c */
	for(i = selmon->clients; i && (!p || !pc); i = i->next) {
		if(i->next == selmon->sel)
			p = i;
		if(i->next == c)
			pc = i;
	}

	/* swap c and selmon->sel selmon->clients in the selmon->clients list */
	if(c && c != selmon->sel) {
		Client *temp = selmon->sel->next==c?selmon->sel:selmon->sel->next;
		selmon->sel->next = c->next==selmon->sel?c:c->next;
		c->next = temp;

		if(p && p != c)
			p->next = c;
		if(pc && pc != selmon->sel)
			pc->next = selmon->sel;

		if(selmon->sel == selmon->clients)
			selmon->clients = c;
		else if(c == selmon->clients)
			selmon->clients = selmon->sel;

		arrange(selmon);
	}
}

/* Attach Direction */
void attachbottom(Client *c) {
    Client **tc;
    c->next = NULL;
    for (tc = &c->mon->clients; *tc; tc = &(*tc)->next);
    *tc = c;
}


/* Restart dwm */
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>

char *get_dwm_path(){
    struct stat s;
    int r, length, rate = 42;
    char *path = NULL;

    if(lstat("/proc/self/exe", &s) == -1){
        perror("lstat:");
        return NULL;
    }

    length = s.st_size + 1 - rate;

    do{
        length+=rate;

        free(path);
        path = malloc(sizeof(char) * length);

        if(path == NULL){
            perror("malloc:");
            return NULL;
        }

        r = readlink("/proc/self/exe", path, length);

        if(r == -1){
            perror("readlink:");
            return NULL;
        }
    }while(r >= length);

    path[r] = '\0';

    return path;
}

void restart(const Arg *arg) {
    char *const argv[] = {get_dwm_path(), NULL};

    if(argv[0] == NULL){
        return;
    }

    execv(argv[0], argv);
}


/* Fibonacci Layout - Spiral & Dwindle */
void fibonacci(Monitor *mon, int s) {
	unsigned int i, n, nx, ny, nw, nh;
	Client *c;

	for(n = 0, c = nexttiled(mon->clients); c; c = nexttiled(c->next), n++);
	if(n == 0)
		return;
	
	nx = mon->wx;
	ny = 0;
	nw = mon->ww;
	nh = mon->wh;
	
	for(i = 0, c = nexttiled(mon->clients); c; c = nexttiled(c->next)) {
		if((i % 2 && nh / 2 > 2 * c->bw)
		   || (!(i % 2) && nw / 2 > 2 * c->bw)) {
			if(i < n - 1) {
				if(i % 2)
					nh /= 2;
				else
					nw /= 2;
				if((i % 4) == 2 && !s)
					nx += nw;
				else if((i % 4) == 3 && !s)
					ny += nh;
			}
			if((i % 4) == 0) {
				if(s)
					ny += nh;
				else
					ny -= nh;
			}
			else if((i % 4) == 1)
				nx += nw;
			else if((i % 4) == 2)
				ny += nh;
			else if((i % 4) == 3) {
				if(s)
					nx += nw;
				else
					nx -= nw;
			}
			if(i == 0)
			{
				if(n != 1)
					nw = mon->ww * mon->mfact;
				ny = mon->wy;
			}
			else if(i == 1)
				nw = mon->ww - nw;
			i++;
		}
		resize(c, nx, ny, nw - 2 * c->bw, nh - 2 * c->bw, False);
	}
}

void dwindle(Monitor *mon) {
	fibonacci(mon, 1);
}

void spiral(Monitor *mon) {
	fibonacci(mon, 0);
}

